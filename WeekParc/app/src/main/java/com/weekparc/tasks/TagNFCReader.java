package com.weekparc.tasks;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;

import com.weekparc.model.Error;
import com.weekparc.model.ResponseCode;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class TagNFCReader extends AsyncTask<Void,Float,Boolean> {
    private String reponse="";
    private TagNFCReaderListener listener=null;
    private Tag tag=null;


    public TagNFCReader(Tag tag,TagNFCReaderListener listener){
        this.listener = listener;
        this.tag=tag;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        Ndef ndef = Ndef.get(tag);
        if (ndef == null) {
            return false;
        }

        NdefMessage ndefMessage = ndef.getCachedNdefMessage();

        NdefRecord[] records = ndefMessage.getRecords();
        for (NdefRecord ndefRecord : records) {
            if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                try {
                    reponse = readText(ndefRecord);
                    return true;
                } catch (UnsupportedEncodingException e) {

                }
            }
        }
        return false;
    }

    private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */

        byte[] payload = record.getPayload();

        // Get the Text Encoding
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

        // Get the Language Code
        int languageCodeLength = payload[0] & 0063;

        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
        // e.g. "en"

        // Get the Text
        return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
    }

        @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(listener != null){
            if(aBoolean){
                listener.readTagDidSuccess(reponse);
            }
            else{
            }
        }
    }

    public interface TagNFCReaderListener{
        void readTagDidSuccess(String message);
    }
}
