package com.weekparc.tasks;

import android.os.AsyncTask;

import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.ResponseCode;
import com.weekparc.model.Error;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class ModelCacheOperation<T> extends AsyncTask<Void,Float,Boolean> {

    // ===========================================================
    // Fields
    // ===========================================================

    private ModelCacheOperationListener listener;

    private ServiceAtlas.ServiceType serviceType;
    private Map<String, String> params;
    private T reponseObject;
    private String errorcause="";


    // ===========================================================
    // Constructors
    // ===========================================================
    public ModelCacheOperation(Map<String,String> params,ServiceAtlas.ServiceType serviceType,ModelCacheOperationListener<T> listener){
        super();
        this.params = (params!=null) ? params : new HashMap<String,String>();
        this.serviceType = serviceType;
        this.listener = listener;
    }

    // ===========================================================
    // Interfaces methods
    // ===========================================================

    @Override
    protected void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try{
            reponseObject = (T) ServiceAtlas.getDataFromLocalDatabase(params,serviceType);
        }
        catch(Exception ex){
            ex.printStackTrace();
            errorcause ="Cache operation - "+ex.getMessage();
            return false;
        }
        finally{
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(listener != null){
            if(aBoolean){
                listener.modelCacheOperationListenerDidSucced(this,reponseObject);
            }
            else{
                Error err=new Error(ResponseCode.LOADING_FROM_CACHE_ERROR);
                err.setErrorcause(errorcause);
                listener.modelCacheOperationListenerDidFail(this,err);
            }
        }
    }

    public interface ModelCacheOperationListener<T> {
        void modelCacheOperationListenerDidSucced(ModelCacheOperation task, T response);
        void modelCacheOperationListenerDidFail(ModelCacheOperation task, Error error);
    }
}
