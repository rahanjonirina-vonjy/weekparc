package com.weekparc.tasks;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;
import android.util.Log;


import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.ResponseCode;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import com.weekparc.model.Error;
import com.weekparc.model.User;

/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class ModelNetworkOperation<T> extends AsyncTask<Void,Float,Boolean>{

    // ===========================================================
    // Constants
    // ===========================================================
    private static final int DEFAULT_TIMEOUT_MILLIS = 10000;

    // ===========================================================
    // Fields
    // ===========================================================

    private ModelNetworkOperationListener<T> listener;
    private String jsonResponse="";
    private Map<String,String> params;
    private int responseCode= ResponseCode.OK;
    private String errorCause="";
    private ServiceAtlas.ServiceType serviceType;

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods from SuperClass
    // ===========================================================
    public ModelNetworkOperation(Map<String,String> params,ServiceAtlas.ServiceType serviceType,ModelNetworkOperationListener listener){
        super();
        this.params = (params!=null) ? params : new HashMap<String,String>();
        this.serviceType = serviceType;
        this.listener=listener;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected Boolean doInBackground(Void... voids) {
        try{
            if(!isConnectedToInternet())
                throw new Error(ResponseCode.NETWORK_DISABLE);

            StringBuilder postData = new StringBuilder();

            for (Map.Entry<String,String> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
            }

            String serviceUrl=ServiceAtlas.getUrlForService(serviceType);
            String method = ServiceAtlas.getHttpMethodForService(serviceType);

            serviceUrl+="?"+postData;

            URL url = new URL(serviceUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod(method);

            addAuthorization(connection);
            addUUID(connection);

            connection.setUseCaches(false);
            connection.setDoInput(true);

            if(method=="POST") {

                postData = new StringBuilder();
                postData.append("{");

                for (Map.Entry<String,String> param : params.entrySet()) {
                    if (postData.length() != 1) postData.append(',');
                    postData.append("\""+URLEncoder.encode(param.getKey(), "UTF-8")+"\"");
                    postData.append(":");
                    postData.append("\""+URLEncoder.encode(param.getValue(), "UTF-8")+"\"");
                }
                postData.append("}");

                Log.d("string", postData.toString());
                connection.setRequestProperty("Content-Length", String.valueOf(postData.toString().getBytes("UTF-8").length));

                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    wr.write(postData.toString().getBytes("UTF-8"));
                    wr.close();
                }
            }

            connection.setConnectTimeout(DEFAULT_TIMEOUT_MILLIS);
            connection.setReadTimeout(DEFAULT_TIMEOUT_MILLIS);

            connection.connect();

            int responseCode = connection.getResponseCode();
            if (responseCode < 200 || responseCode >= 400) {
               throw new Error(responseCode);
            }

            this.jsonResponse=inputStreamToString(connection.getInputStream());

        }
        catch(Error erreur){
            responseCode = erreur.getResponseCode();
            return false;
        }
        catch(Exception ex){
            ex.printStackTrace();
            errorCause = ex.getMessage();
            responseCode= ResponseCode.INTERNAL_SERVER_ERROR;
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(listener!=null){
            if(aBoolean) {
                try {
                    T objectResponse = (T)  ServiceAtlas.parseFromJson(jsonResponse, serviceType);
                    listener.modelNetworkOperationListenerDidSucced(this, objectResponse);
                }
                catch(Exception ex){
                    ex.printStackTrace();
                    Error error=new Error(ResponseCode.PARSING_ERROR);
                    error.setErrorcause(ex.getMessage());
                    listener.modelNetworkOperationListenerDidFail(this, error);
                }
            }
            else{
                Error error = new Error(responseCode);
                error.setErrorcause(errorCause);
                listener.modelNetworkOperationListenerDidFail(this, error);
            }
        }
    }

    // ===========================================================
    // Private Methods
    // ===========================================================

    private void addUUID(HttpURLConnection connection){
        //TelephonyManager manager = (TelephonyManager) ApplicationConfig.applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
        //connection.setRequestProperty("uuid","cfghhfgbhfghnfhnfghn"/*manager.getDeviceId()*/);

    }
    public static String inputStreamToString(InputStream inputStream)throws Exception {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            Error error =  new Error(ResponseCode.READ_DATA_ERROR);
            error.setErrorcause(e.getMessage());
            throw error;
        } finally {
            try {
                bufferedReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String res = stringBuilder.toString();

        return res;
    }
    private static void addAuthorization(HttpURLConnection connection) {

        User currentUser = ApplicationConfig.currentUser;
        if (null == currentUser) {
            return;
        }

        if (null == currentUser.getLogin() || null == currentUser.getPassword()) {
            return;
        }

        String credentials = currentUser.getLogin() + ":" + currentUser.getPassword();
        String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
        connection.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);
    }
    private static boolean isConnectedToInternet() {
        ConnectivityManager cm = (ConnectivityManager) ApplicationConfig.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected());
    }

    public interface ModelNetworkOperationListener<T> {
        void modelNetworkOperationListenerDidSucced(ModelNetworkOperation task, T response);
        void modelNetworkOperationListenerDidFail(ModelNetworkOperation task, Error error);
    }
}
