package com.weekparc.manager;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.Error;
import com.weekparc.model.Visiteur;
import com.weekparc.tasks.ModelNetworkOperation;

/**
 * Created by hrahanjo on 4/23/2017.
 */

public class LoginManager extends ManagerModel<Visiteur> implements ModelNetworkOperation.ModelNetworkOperationListener<Visiteur> {
    public LoginManager(ManagerListener<Visiteur> listener) {
        super(listener);
    }

    public void send(String pseudo,String passe){
        start();
        params.put("login",pseudo);
        params.put("passe",passe);
        networkOperation  =new ModelNetworkOperation<Visiteur>(params, ServiceAtlas.ServiceType.ServiceLogin,this);
        networkOperation.execute();
    }

    @Override
    public void modelNetworkOperationListenerDidSucced(ModelNetworkOperation task, Visiteur response) {
        ApplicationConfig.visiteur = response;
        if(listener!=null)
            listener.ManagerListenerDidSucced(this,response);
    }

    @Override
    public void modelNetworkOperationListenerDidFail(ModelNetworkOperation task, Error error) {
        if(listener!=null)
            listener.ManagerListenerDidFail(this,error);
    }
}
