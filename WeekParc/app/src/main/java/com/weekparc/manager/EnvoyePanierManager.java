package com.weekparc.manager;

import android.util.Log;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.Error;
import com.weekparc.model.ResponseCode;
import com.weekparc.tasks.ModelNetworkOperation;
import com.weekparc.utils.Utility;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class EnvoyePanierManager extends ManagerModel implements ModelNetworkOperation.ModelNetworkOperationListener{

        public EnvoyePanierManager(ManagerListener listener) {
        super(listener);
    }

    public void send(List<Billet> listeBillet,int idvisiteur)  {
        start();
        List<String> splitBilletToString = Utility.getDataForWebService(listeBillet);
        params.put("idactivite",splitBilletToString.get(0));
        params.put("effectif",splitBilletToString.get(1));
        params.put("idvisiteur",idvisiteur+"");
        /*networkOperation = new ModelNetworkOperation(params, ServiceAtlas.ServiceType.ServiceEnvoyePanier,this);
        networkOperation.execute();*/
        try {

            String link=ServiceAtlas.getUrlForService(ServiceAtlas.ServiceType.ServiceEnvoyePanier)+"?idactivite="+splitBilletToString.get(0)
                    +"&effectif="+splitBilletToString.get(1)+"&idvisiteur="+idvisiteur;
            URL url = new URL("http://ultrav.alwaysdata.net/week-parc/activites.php");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod("GET");


            connection.setUseCaches(false);
            connection.setDoInput(true);

            connection.setConnectTimeout(10000);
            connection.setReadTimeout(10000);

            connection.connect();

            int responseCode = connection.getResponseCode();
            if (responseCode < 200 || responseCode >= 400) {
                throw new Error(responseCode);
            }

            String reponse = ModelNetworkOperation.inputStreamToString(connection.getInputStream());
        }
        catch(Exception ex){
            Error error=new Error(ResponseCode.PARSING_ERROR);
            error.setErrorcause(ex.getMessage());
            if(listener!=null)
                listener.ManagerListenerDidFail(this,error);
        }
    }


        @Override
        public void modelNetworkOperationListenerDidSucced(ModelNetworkOperation task, Object response) {
            if(listener!=null)
                listener.ManagerListenerDidSucced(this,response);
        }

        @Override
        public void modelNetworkOperationListenerDidFail(ModelNetworkOperation task, Error error) {
            if(listener!=null)
                listener.ManagerListenerDidFail(this,error);
        }

}
