package com.weekparc.manager;

import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.Error;
import com.weekparc.tasks.ModelCacheOperation;
import com.weekparc.tasks.ModelNetworkOperation;

import java.util.HashMap;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class AjoutActiviteManager extends ManagerModel implements ModelCacheOperation.ModelCacheOperationListener {

    public AjoutActiviteManager(ManagerListener listener) {
        super(listener);
    }

    public void refresh(int idactivite){
        super.start();
        params =  new HashMap();
        params.put("idactivite",idactivite+"");
        params.put("idvisiteur","1");
        params.put("effectif","1");
        cacheOperation = new ModelCacheOperation(params, ServiceAtlas.ServiceType.ServiceAjoutActivite,this);
        cacheOperation.execute();
    }

    @Override
    public void modelCacheOperationListenerDidSucced(ModelCacheOperation task, Object response) {
        if(listener!=null)
            listener.ManagerListenerDidSucced(this,response);
    }

    @Override
    public void modelCacheOperationListenerDidFail(ModelCacheOperation task, Error error) {
        if(listener!=null)
            listener.ManagerListenerDidFail(this,error);
    }
}
