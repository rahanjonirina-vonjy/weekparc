package com.weekparc.manager;

/**
 * Created by Vonjy-Dev on 19/01/16.
 */


import com.weekparc.tasks.ModelCacheOperation;
import com.weekparc.tasks.ModelNetworkOperation;

import java.util.HashMap;
import java.util.Map;
import com.weekparc.model.Error;

public abstract class ManagerModel<T> {



    protected ModelNetworkOperation<T> networkOperation;
    protected ModelCacheOperation<T> cacheOperation;
    protected ManagerListener<T>  listener;

    protected Map<String,String> params;

    public ManagerModel(ManagerListener<T>  listener){
        this.listener = listener;
        params = new HashMap<String,String>();
    }

    public void start(){
        if(networkOperation!=null)
            if(!networkOperation.isCancelled())
                networkOperation.cancel(true);
        if(cacheOperation!=null)
            if(!cacheOperation.isCancelled())
                cacheOperation.cancel(true);
    }
    public interface ManagerListener<T>{
        void ManagerListenerDidSucced(ManagerModel manager, T reponse);
        void ManagerListenerDidFail(ManagerModel manager, Error error);
    }
}
