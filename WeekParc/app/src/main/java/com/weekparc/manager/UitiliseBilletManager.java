package com.weekparc.manager;

import com.weekparc.model.Error;
import com.weekparc.tasks.ModelNetworkOperation;

import static com.weekparc.application.ServiceAtlas.ServiceType.ServiceUtiliserBillet;

/**
 * Created by hrahanjo on 4/25/2017.
 */

public class UitiliseBilletManager extends ManagerModel implements ModelNetworkOperation.ModelNetworkOperationListener {
    public UitiliseBilletManager(ManagerListener listener) {
        super(listener);
    }
    public void send(int idbillet,int effectif){
        params.put("idbillet",idbillet+"");
        params.put("effectif",effectif+"");
        networkOperation = new ModelNetworkOperation(params,ServiceUtiliserBillet,this);
        networkOperation.execute();
    }

    @Override
    public void modelNetworkOperationListenerDidSucced(ModelNetworkOperation task, Object response) {

    }

    @Override
    public void modelNetworkOperationListenerDidFail(ModelNetworkOperation task, Error error) {

    }
}
