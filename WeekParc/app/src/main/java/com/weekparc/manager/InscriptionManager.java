package com.weekparc.manager;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.Error;
import com.weekparc.model.Visiteur;
import com.weekparc.tasks.ModelNetworkOperation;

/**
 * Created by hrahanjo on 4/23/2017.
 */

public class InscriptionManager extends ManagerModel<Visiteur> implements ModelNetworkOperation.ModelNetworkOperationListener<Visiteur> {
    public InscriptionManager(ManagerListener<Visiteur> listener) {
        super(listener);
    }

    public void send(Visiteur visiteur){
        start();
        params.put("nom",visiteur.getNom());
        params.put("prenom",visiteur.getPrenom());
        params.put("login",visiteur.getLogin());
        params.put("passe",visiteur.getPasse());
        networkOperation = new ModelNetworkOperation<>(params, ServiceAtlas.ServiceType.ServiceInscription,this);
        networkOperation.execute();
    }


    @Override
    public void modelNetworkOperationListenerDidSucced(ModelNetworkOperation task, Visiteur response) {
        ApplicationConfig.visiteur = response;
        if(listener!=null)
            listener.ManagerListenerDidSucced(this,response);
    }

    @Override
    public void modelNetworkOperationListenerDidFail(ModelNetworkOperation task, Error error) {
        if(listener!=null)
            listener.ManagerListenerDidFail(this,error);
    }
}
