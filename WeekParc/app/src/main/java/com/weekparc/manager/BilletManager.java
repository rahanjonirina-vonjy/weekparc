package com.weekparc.manager;

import android.os.AsyncTask;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.model.Activite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hrahanjo on 4/22/2017.
 */

public class BilletManager {
    private static List<Billet> billets= new ArrayList<Billet>();
    public Billet getBilletByActivite(int idactivite){
        for(Billet billet:billets){
            if(billet.getActivite().getIdactivite()==idactivite)
                return billet;
        }
        return null;
    }

    public Billet augmentBillet(Activite activite){
        Billet billet = getBilletByActivite(activite.getIdactivite());
        if(billet!=null){
            billet.setEffectif(billet.getEffectif()+1);
        }else{
            billet = new Billet();
            billet.setEffectif(1);
            billet.setActivite(activite);
            billets.add(billet);
        }
        return billet;
    }

    public Billet rediureBillet(int idactivite){
        Billet billet = getBilletByActivite(idactivite);
        if(billet!=null){
            billet.setEffectif(billet.getEffectif()-1);
        }
        return billet;
    }
    public List<Billet> getAllBillet(){
        return billets;
    }
    public void deleteAll(){
        billets = new ArrayList<>();
    }
}
