package com.weekparc.manager;

import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.Activite;
import com.weekparc.model.BaseModel;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by hrahanjo on 4/7/2017.
 */

public class Billet extends BaseModel {

    private int idbillet;
    private int idactivite;
    private int effectif;
    private int idpanier;
    private Activite activite;

    @Override
    public Object parseFromJson(String json, ServiceAtlas.ServiceType servicetype) {
        return null;
    }

    public int getIdbillet() {
        return idbillet;
    }

    public void setIdbillet(int idbillet) {
        this.idbillet = idbillet;
    }

    public int getIdactivite() {
        return idactivite;
    }

    public void setIdactivite(int idactivite) {
        this.idactivite = idactivite;
    }

    public int getEffectif() {
        return effectif;
    }

    public void setEffectif(int effectif) {
        this.effectif = effectif;
    }

    public int getIdpanier() {
        return idpanier;
    }

    public void setIdpanier(int idpanier) {
        this.idpanier = idpanier;
    }

    public Activite getActivite() {
        return activite;
    }

    public void setActivite(Activite activite) {
        this.activite = activite;
    }


}
