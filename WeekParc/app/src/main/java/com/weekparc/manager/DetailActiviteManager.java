package com.weekparc.manager;

import android.app.Application;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.Activite;
import com.weekparc.model.Error;
import com.weekparc.tasks.ModelNetworkOperation;
import com.weekparc.viewcomponent.ActiviteHomeViewItem;
import com.weekparc.viewcomponent.UrlImageView;

import java.util.HashMap;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class DetailActiviteManager extends ManagerModel<Activite> implements ModelNetworkOperation.ModelNetworkOperationListener<Activite> {

    public DetailActiviteManager(ManagerListener listener) {
        super(listener);
    }

    public void refresh(Activite activite){
        start();
        params = new HashMap<String,String>();
        params.put("idactivite",activite.getIdactivite()+"");
        params.put("idvisiteur", ApplicationConfig.visiteur.getIdvisiteur()+"");
        if(activite.getTag_nfc()!=null) {
            params.put("tag_nfc", activite.getTag_nfc());
        }

        networkOperation = new ModelNetworkOperation(params,ServiceAtlas.ServiceType.ServiceDetailActivite,this);
        networkOperation.execute();
    }

    @Override
    public void modelNetworkOperationListenerDidSucced(ModelNetworkOperation task, Activite response) {
        if(listener!=null)
            listener.ManagerListenerDidSucced(this,response);
    }

    @Override
    public void modelNetworkOperationListenerDidFail(ModelNetworkOperation task, Error error) {
        if(listener!=null)
            listener.ManagerListenerDidFail(this,error);
    }
}
