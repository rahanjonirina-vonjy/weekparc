package com.weekparc.manager;

import android.content.Context;
import android.os.AsyncTask;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.dao.ActiviteDao;
import com.weekparc.model.Activite;
import com.weekparc.model.Error;
import com.weekparc.tasks.ModelCacheOperation;
import com.weekparc.tasks.ModelNetworkOperation;

import java.util.List;

/**
 * Created by hrahanjo on 4/4/2017.
 */

public class ListeActivitesManager extends ManagerModel<List<Activite>> implements ModelNetworkOperation.ModelNetworkOperationListener<List<Activite>>,ModelCacheOperation.ModelCacheOperationListener<List<Activite>> {

    private Context context;

    public ListeActivitesManager(ManagerListener<List<Activite>> listener) {
        super(listener);
    }

    public void refresh(String libelle,Context context){
        start();
        params.put("libelle",libelle==null ? "" : libelle);
        this.context=context;
        networkOperation =  new ModelNetworkOperation<List<Activite>>(params, ServiceAtlas.ServiceType.ServiceListeActivite,this);
        this.networkOperation.execute();
    }


    @Override
    public void modelNetworkOperationListenerDidSucced(ModelNetworkOperation task,final List<Activite> response) {
        if(listener!=null)
            listener.ManagerListenerDidSucced(this,response);

        ActiviteDao dao = ActiviteDao.getInstance(context);
        try {

            int nb =0;
            for (Activite activite : response) {
                dao.createActivite(activite);
                nb++;
            }

        }
        catch(Exception ex){
            Error err =new Error(1);
            err.setErrorcause(ex.getMessage());
            listener.ManagerListenerDidFail(this,err);
        }


    }

    @Override
    public void modelNetworkOperationListenerDidFail(ModelNetworkOperation task, Error error) {
         cacheOperation = new ModelCacheOperation<>(params,ServiceAtlas.ServiceType.ServiceListeActivite,this);
        cacheOperation.execute();
        //listener.ManagerListenerDidFail(this,error);

    }

    @Override
    public void modelCacheOperationListenerDidSucced(ModelCacheOperation task, List<Activite> response) {
        if(listener!=null)
            listener.ManagerListenerDidSucced(this,response);

    }

    @Override
    public void modelCacheOperationListenerDidFail(ModelCacheOperation task, Error error) {
        if(listener!=null)
            listener.ManagerListenerDidFail(this,error);
    }
}
