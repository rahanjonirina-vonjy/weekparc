package com.weekparc.model;

/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class Error extends Exception {

    private int responseCode;
    private String errorcause;

    public Error(int code){
        this.setResponseCode(code);
    }
    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getErrorcause() {
        return errorcause;
    }

    public void setErrorcause(String errorcause) {
        this.errorcause = errorcause;
    }
}
