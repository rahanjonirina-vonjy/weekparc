package com.weekparc.model;

/**
 * Created by hrahanjo on 4/25/2017.
 */

public class Billet {
    private int idbillet;
    private int idpanier;
    private int idactivite;
    private int effectif;
    private int consommer;

    public int getIdbillet() {
        return idbillet;
    }

    public void setIdbillet(int idbillet) {
        this.idbillet = idbillet;
    }

    public int getIdpanier() {
        return idpanier;
    }

    public void setIdpanier(int idpanier) {
        this.idpanier = idpanier;
    }

    public int getIdactivite() {
        return idactivite;
    }

    public void setIdactivite(int idactivite) {
        this.idactivite = idactivite;
    }

    public int getEffectif() {
        return effectif;
    }

    public void setEffectif(int effectif) {
        this.effectif = effectif;
    }

    public int getConsommer() {
        return consommer;
    }

    public void setConsommer(int consommer) {
        this.consommer = consommer;
    }
}
