package com.weekparc.model;

/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class ResponseCode {
    public static final int OK=200;
    public static final int INTERNAL_SERVER_ERROR=405;
    public static final int INTERNAL_SERVER_ERROR2=406;
    public static final int UNEXISTE_ACCOUNT=403;
    public static final int UNCORRECT_CHAMP=402;
    public static final int NETWORK_DISABLE=410;
    public static final int NO_URL_FOR_SERVICE=411;
    public static final int PARSING_ERROR=1;
    public static final int READ_DATA_ERROR=2;
    public static final int LOADING_FROM_CACHE_ERROR=413;
}
