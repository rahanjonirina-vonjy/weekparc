package com.weekparc.model;

import android.widget.VideoView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.weekparc.application.ServiceAtlas;

/**
 * Created by hrahanjo on 4/23/2017.
 */

public class Visiteur extends BaseModel {

    private int idvisiteur;
    private String nom;
    private String prenom;
    private String login;
    private String passe;

    @Override
    public Object parseFromJson(String json, ServiceAtlas.ServiceType servicetype) {
        Gson gson=new GsonBuilder().create();
        return gson.fromJson(json,Visiteur.class);
    }

    public int getIdvisiteur() {
        return idvisiteur;
    }

    public void setIdvisiteur(int idvisiteur) {
        this.idvisiteur = idvisiteur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasse() {
        return passe;
    }

    public void setPasse(String passe) {
        this.passe = passe;
    }
}
