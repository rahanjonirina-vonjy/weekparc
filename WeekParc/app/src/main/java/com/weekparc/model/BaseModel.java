package com.weekparc.model;


import com.weekparc.application.ServiceAtlas;

import java.io.Serializable;

/**
 * Created by hrahanjo on 2/21/2017.
 */

public abstract class  BaseModel implements Serializable {
    public abstract Object parseFromJson(String json, ServiceAtlas.ServiceType servicetype);
}
