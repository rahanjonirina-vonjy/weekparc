package com.weekparc.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.weekparc.application.ServiceAtlas;

import java.util.List;

/**
 * Created by hrahanjo on 4/4/2017.
 */

public class Activite extends BaseModel {

    private int idactivite;
    private String libelle;
    private String image;
    private Double prix;
    private String portfollio;
    private String description;
    private String tag_nfc;
    private Billet billet;
    private int note;

    @Override
    public Object parseFromJson(String json, ServiceAtlas.ServiceType servicetype) {
        Gson  gson=new GsonBuilder().create();
        return servicetype == ServiceAtlas.ServiceType.ServiceDetailActivite ? gson.fromJson(json,Activite.class) :  gson.fromJson(json,new TypeToken<List<Activite>>(){}.getType());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPortfollio() {
        return portfollio;
    }

    public void setPortfollio(String portfollio) {
        this.portfollio = portfollio;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getIdactivite() {
        return idactivite;
    }

    public void setIdactivite(int idacivite) {
        this.idactivite = idacivite;
    }

    public String getTag_nfc() {
        return tag_nfc;
    }

    public void setTag_nfc(String tag_nfc) {
        this.tag_nfc = tag_nfc;
    }

    public Billet getBillet() {
        return billet;
    }

    public void setBillet(Billet billet) {
        this.billet = billet;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }
}
