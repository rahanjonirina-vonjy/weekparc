package com.weekparc.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.manager.Billet;

import java.util.List;

import static com.weekparc.application.ServiceAtlas.ServiceType.ServiceEnvoyePanier;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class Panier extends BaseModel {
    private int idpanier;
    private List<Billet> billets;
    private int etat;
    private Double total;

    @Override
    public Object parseFromJson(String json, ServiceAtlas.ServiceType servicetype) {
        Gson gson=new GsonBuilder().create();
        if(servicetype==ServiceEnvoyePanier)
            return json;
        return gson.fromJson(json,Panier.class);
    }

    public int getIdpanier() {
        return idpanier;
    }

    public void setIdpanier(int idpanier) {
        this.idpanier = idpanier;
    }


    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<Billet> getBillets() {
        return billets;
    }

    public void setBillets(List<Billet> billets) {
        this.billets = billets;
    }
}
