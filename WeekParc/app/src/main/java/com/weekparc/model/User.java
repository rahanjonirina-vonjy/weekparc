package com.weekparc.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.weekparc.application.ServiceAtlas;

/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class User extends BaseModel {
    // ===========================================================
    // Fields
    // ===========================================================

    private String idprofil;
    private String nom;
    private String prenom;
    private String idgenre;
    private String login;
    private String password;

    // ===========================================================
    // Public static methods
    // ===========================================================
    public Object parseFromJson(String json, ServiceAtlas.ServiceType serviceType){
        Gson gson=new GsonBuilder().create();
        return gson.fromJson(json,User.class);
    }

    public User duplicate(){
        User newD=new User();
        newD.setIdprofil(idprofil);
        newD.setNom(nom);
        newD.setPrenom(prenom);
        newD.setIdgenre(idgenre);
        newD.setLogin(login);
        newD.setPassword(password);

        return newD;
    }

    public String getIdprofil() {
        return idprofil;
    }

    public void setIdprofil(String idprofil) {
        this.idprofil = idprofil;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getIdgenre() {
        return idgenre;
    }

    public void setIdgenre(String idgenre) {
        this.idgenre = idgenre;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
