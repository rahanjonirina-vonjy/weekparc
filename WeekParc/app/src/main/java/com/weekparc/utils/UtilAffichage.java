package com.weekparc.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.weekparc.R;


/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class UtilAffichage {

    public static void setfont(TextView text, String fonFamily, AssetManager asset){
        Typeface myCustomFont = Typeface.createFromAsset(asset,fonFamily);
        text.setTypeface(myCustomFont);
    }
    public static Typeface getfont(String fonFamily,AssetManager asset){
        return Typeface.createFromAsset(asset,fonFamily);
    }
    public static void dialogcreate(Context context,String title,String message,int button){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context);
        dlgAlert.setMessage(message);
        dlgAlert.setTitle(title);
        dlgAlert.setPositiveButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dlgAlert.setCancelable(false);
        dlgAlert.create().show();
    }
    public static void hideKeyBoard(Context context,View view){
        InputMethodManager inputMethodManager = (InputMethodManager)  context.getSystemService(context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static String formatSize(double size){
        /*DecimalFormat myNumberFormat = new DecimalFormat("0.00");
        return myNumberFormat.format(size);*/
        return String.format("%.2f", size);
    }

}
