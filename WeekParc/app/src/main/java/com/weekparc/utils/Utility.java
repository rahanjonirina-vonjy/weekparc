package com.weekparc.utils;

import com.weekparc.manager.Billet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hrahanjo on 2/28/2017.
 */

public class Utility {
    public static String setFirstMaj(String str){
        return str.toUpperCase().substring(0, 1)+""+str.substring(1);
    }
    public static List<String> getDataForWebService(List<Billet> listeBillet){
        //param idactivite=1,4,5&effectif=4,5,6&idvisiteur=6
        List<String> resultat = new ArrayList<>();
        String activite ="";
        String effectif = "";
        int index = 0;
        for (Billet billet : listeBillet){
            if(index == listeBillet.size()-1){
                activite += billet.getActivite().getIdactivite()+"";
                effectif += billet.getEffectif()+"";
            }
            else {
                activite += billet.getActivite().getIdactivite() + ",";
                effectif += billet.getEffectif() + ",";
            }
            index++;
        }
        resultat.add(activite);
        resultat.add(effectif);
        return resultat;
    }
}
