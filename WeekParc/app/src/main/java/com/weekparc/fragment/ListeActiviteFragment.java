package com.weekparc.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.weekparc.R;
import com.weekparc.activity.DetailActiviteActivity;
import com.weekparc.adapter.ListeActiviteAdapter;
import com.weekparc.manager.ListeActivitesManager;
import com.weekparc.manager.ManagerModel;
import com.weekparc.model.Activite;
import com.weekparc.model.Error;
import com.weekparc.utils.UtilAffichage;
import com.weekparc.viewcomponent.ActiviteHomeViewItem;
import com.weekparc.viewcomponent.CarrouselView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListeActiviteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListeActiviteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListeActiviteFragment extends Fragment implements ActiviteHomeViewItem.ActiviteItemEventListener, ManagerModel.ManagerListener<List<Activite>> {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView listeview;
    private ListeActiviteAdapter adapter;
    ArrayAdapter<String> adapter1;

    private ListeActivitesManager manager;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListeActiviteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListeActiviteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListeActiviteFragment newInstance(String param1, String param2) {
        ListeActiviteFragment fragment = new ListeActiviteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rep = inflater.inflate(R.layout.fragment_liste_activite, container, false);
        try{

            listeview = (ListView) rep.findViewById(R.id.liste_activite);

            adapter = new ListeActiviteAdapter(null, this,this.getContext());
            listeview.setAdapter(adapter);

            manager = new ListeActivitesManager(this);
            manager.start();
        }
        catch(Exception ex){
            UtilAffichage.dialogcreate(getContext(),"Erreur",ex.getMessage(),1);
        }
        return rep;
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onResume() {
        super.onResume();
        manager.refresh("",this.getContext());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void didClicked(Activite item) {
        DetailActiviteActivity.affiche(this.getContext(),item);
    }

    @Override
    public void ManagerListenerDidSucced(ManagerModel manager, List<Activite> reponse) {
        try {
            adapter.setActivites(reponse);
            List<String> images=new ArrayList<>();
            for(Activite activite:reponse)
                images.add(activite.getImage());
            ((CarrouselView)getView().findViewById(R.id.carrousel_view)).affecter(images);
            adapter.notifyDataSetInvalidated();
        }
        catch(Exception ex){
            UtilAffichage.dialogcreate(this.getContext(),"Erreur",ex.getMessage(),1);
        }
    }

    @Override
    public void ManagerListenerDidFail(ManagerModel manager, Error error) {
        UtilAffichage.dialogcreate(this.getContext(),"Erreur "+error.getResponseCode(),error.getErrorcause(),1);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
