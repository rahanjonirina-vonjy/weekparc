package com.weekparc.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.weekparc.R;
import com.weekparc.activity.DetailActiviteActivity;
import com.weekparc.adapter.MesActiviteListeAdapter;
import com.weekparc.manager.Billet;
import com.weekparc.manager.BilletManager;
import com.weekparc.manager.EnvoyePanierManager;
import com.weekparc.manager.ManagerModel;
import com.weekparc.model.Activite;
import com.weekparc.model.Error;
import com.weekparc.utils.UtilAffichage;
import com.weekparc.viewcomponent.ActiviteHomeViewItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MesActivitesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MesActivitesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MesActivitesFragment extends Fragment implements ActiviteHomeViewItem.ActiviteItemEventListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView prixTotal=null;
    private Button envoyer = null;
    private ListView listeActivites = null;
    private MesActiviteListeAdapter adapter = null;
    private MesActivitesFragment self=this;
    private EnvoyePanierManager envoyePanierManager;

    private OnFragmentInteractionListener mListener;
    private BilletManager billetmanager;
    List<Billet> billets;
    private ManagerModel.ManagerListener managerListener = new ManagerModel.ManagerListener<String>() {
        @Override
        public void ManagerListenerDidSucced(ManagerModel manager, String reponse) {
           // UtilAffichage.dialogcreate(self.getContext(),"Erreur", "lol",1);
        }

        @Override
        public void ManagerListenerDidFail(ManagerModel manager, Error error) {
            UtilAffichage.dialogcreate(self.getContext(),"Erreur", error.getErrorcause(),1);
        }
    };

    public MesActivitesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MesActivitesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MesActivitesFragment newInstance(String param1, String param2) {
        MesActivitesFragment fragment = new MesActivitesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rep = inflater.inflate(R.layout.fragment_mes_activites, container, false);
        prixTotal = (TextView) rep.findViewById(R.id.prix_total);
        envoyer = (Button) rep.findViewById(R.id.boutton_envoyer);
        listeActivites = (ListView) rep.findViewById(R.id.mes_activites);
        adapter = new MesActiviteListeAdapter(null,this);
        listeActivites.setAdapter(adapter);
        envoyePanierManager = new EnvoyePanierManager(managerListener);
        billetmanager = new BilletManager();
        envoyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String taille = String.valueOf(billets.size());
                billets=new ArrayList<Billet>();
                adapter.setBillets(billets);
                adapter.notifyDataSetChanged();
                prixTotal.setText("0.0 Rs");
                billetmanager.deleteAll();
                //envoyePanierManager.send(billets, ApplicationConfig.visiteur.getIdvisiteur());
                /*for(Billet billet : adapter.getBillets()) {
                    billetmanager.rediureBillet(billet.getIdactivite());
                }*/

            }
        });

        return rep;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        billets=billetmanager.getAllBillet();
        adapter.setBillets(billets);
        adapter.notifyDataSetChanged();
        Double somme = 0.0;
        for(Billet billet:billets){
            somme+=billet.getEffectif()*billet.getActivite().getPrix();
        }
        prixTotal.setText(somme+" Rs");
    }
    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void didClicked(Activite activite) {
        DetailActiviteActivity.affiche(getContext(),activite);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
