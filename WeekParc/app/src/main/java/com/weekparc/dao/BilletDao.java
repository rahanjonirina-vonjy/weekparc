package com.weekparc.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.manager.Billet;
import com.weekparc.model.Activite;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by hrahanjo on 4/7/2017.
 */

public class BilletDao implements DataAccesser {

    public static final String table = "billet";

    private String[] allColumns = { "idbillet","idactivite","idpanier","effectif"};

    private static BilletDao instance = null;

    private BilletDao(Context context){
        dbHelper = new DbHelper(context);
    }
    public static BilletDao getInstance(Context context){
        if(instance==null)
            instance=new BilletDao(context);
        return instance;
    }

    private SQLiteDatabase database;
    private DbHelper dbHelper;

    @Override
    public Object getObject(Map<String, String> param,ServiceAtlas.ServiceType servicetype) {
        return getAllBillet(null);
    }

    @Override
    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        dbHelper.close();
    }

    public void createBillet(Billet billet) {
        open();
        try {
            ContentValues values = new ContentValues();

            values.put("idpanier", billet.getIdpanier());
            values.put("idactivite", billet.getIdactivite());
            values.put("effectif", billet.getEffectif());

            long insertId = database.insert(table, null, values);
            billet.setIdbillet((int) insertId);
        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            close();
        }
    }

    public void deleteBillet(int idbillet) {
        open();
        try {
            database.delete(table, "idbillet"
                    + " = " + idbillet, null);
        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            close();
        }
    }
    public void deleteAllBillet(){
        open();
        try {
            database.delete(table, null, null);
        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            close();
        }
    }

    public Billet getBilletByActivite(int idactivite){
        open();
        try{
            Cursor cursor = database.query(table, allColumns, "idactivite=?", new String[]{idactivite+""}, null, null, null);

            if(cursor.moveToFirst()){
                return cursorToBillet(cursor);
            }
        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            close();
        }
        return null;
    }
    public List<Billet> getAllBillet(Billet billet) {
        open();
        try {
            List<Billet> activites = new ArrayList<Billet>();

            String where = null;
            List<String> valWhere = new ArrayList<>();

            if(billet!=null){
                where="";
                if(billet.getIdactivite()!=0){
                    where+="idactivite = ?";
                    valWhere.add(billet.getIdactivite()+"");
                }
            }

            Cursor cursor = database.query(table, allColumns, where, valWhere.toArray(new String[]{}), null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Billet comment = cursorToBillet(cursor);
                if(comment.getEffectif()==0)
                    continue;
                activites.add(comment);
                comment.setActivite(ActiviteDao.getInstance(ApplicationConfig.applicationContext).findById(comment.getIdactivite()));
                cursor.moveToNext();
            }
            cursor.close();
            return activites;
        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            close();
        }
    }

    private Billet cursorToBillet(Cursor cursor) {
        Billet activite = new Billet();
        activite.setIdbillet(cursor.getInt(0));
        activite.setIdactivite(cursor.getInt(1));
        activite.setIdpanier(cursor.getInt(2));
        activite.setEffectif(cursor.getInt(3));
        return activite;
    }
}
