package com.weekparc.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.weekparc.model.Activite;
import com.weekparc.model.Panier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hrahanjo on 2/23/2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "WeekparcDb.db";
    public static String DATABASE_SCRIPT_CREATION="";
    private static List<String> tables;
    private static List<String> createTables;

    static{
        tables = new ArrayList<String>();
        createTables = new ArrayList<String>();

        tables.add("panier");
        tables.add("billet");
        tables.add("activite");

        createTables.add("CREATE TABLE "+ PanierDao.table+" (ipanier INTEGER PRIMARY KEY AUTOINCREMENT, etat INTEGER,total DOUBLE);");
        createTables.add("CREATE TABLE "+ BilletDao.table+" (idbillet INTEGER PRIMARY KEY AUTOINCREMENT, idactivite INTEGER,idpanier INTEGER,effectif INTEGER);");
        createTables.add("CREATE TABLE "+ ActiviteDao.table+" (idacivite INTEGER PRIMARY KEY, prix DOUBLE,description TEXT,image TEXT," +
                "libelle TEXT,portfollio TEXT,tag_nfc TEXT);");

    }

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        for(String srt:createTables){
            try{
                db.execSQL(srt);
            }
            catch(Exception ex){

            }
        }
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            for (String table : tables) {
                try {
                     db.execSQL("DROP TABLE IF EXISTS " + table );
                }
                catch(Exception ex){

                }
            }

        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
