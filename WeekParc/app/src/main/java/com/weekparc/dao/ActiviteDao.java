package com.weekparc.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weekparc.application.ServiceAtlas;
import com.weekparc.model.Activite;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by hrahanjo on 4/7/2017.
 */

public class ActiviteDao implements DataAccesser {

    public static final String table = "activite";
    private String[] allColumns = { "idacivite","libelle","image","prix","portfollio","description","tag_nfc" };


    private SQLiteDatabase database;
    private DbHelper dbHelper;
    private static ActiviteDao instance = null;

    private ActiviteDao(Context context){
        dbHelper = new DbHelper(context);
    }
    public static ActiviteDao getInstance(Context context){
        if(instance==null)
            instance=new ActiviteDao(context);
        return instance;
    }



    @Override
    public Object getObject(Map<String, String> param,ServiceAtlas.ServiceType servicetype) {
        if(servicetype== ServiceAtlas.ServiceType.ServiceDetailActivite){
            if(param.containsKey("tag_nfc")){
                return findByTag(param.get("tag_nfc"));
            }
            return findById(Integer.parseInt(param.get("idactivite")));
        }
        return getAllActivite(null);
    }

    @Override
    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        dbHelper.close();
    }


    public void createActivite(Activite activite) {
        open();
        try {
            ContentValues values = new ContentValues();

            values.put("idacivite", activite.getIdactivite());
            values.put("libelle", activite.getLibelle());
            values.put("prix", activite.getPrix());
            values.put("description", activite.getDescription());
            values.put("image", activite.getImage());
            values.put("portfollio", activite.getPortfollio());
            values.put("tag_nfc", activite.getTag_nfc());
            long insertId = database.insert(table, null, values);
        }
        catch(Exception ex){
            throw new RuntimeException("insert act - "+ex.getMessage());
        }
        finally {
            close();
        }
    }

    public void deleteActivite(int idactivite) {
        open();
        try {
            database.delete(table, "idacivite"
                    + " = " + idactivite, null);
        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            close();
        }
    }


    public Activite findByTag(String tag){
        open();
        Cursor cursor=null;
        try {

            cursor = database.query(table, allColumns, "tag_nfc"+"=?", new String[]{tag+""}, null, null, null);

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                return cursorToActiite(cursor);
            }

        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            cursor.close();
            close();
        }
        return null;
    }

    public Activite findById(int idactivite){
        open();
        Cursor cursor=null;
        try {

            cursor = database.query(table, allColumns, "idacivite"+"=?", new String[]{idactivite+""}, null, null, null);

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                return cursorToActiite(cursor);
            }

        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            cursor.close();
            close();
        }
        return null;
    }

    public List<Activite> getAllActivite(Activite activite) {
        open();
        try {
            List<Activite> activites = new ArrayList<Activite>();

            String where = null;
            List<String> valWhere = new ArrayList<>();

            if(activite!=null){
                where="";
                if(activite.getLibelle()!=null){
                    where+="libelle like '%?%'";
                    valWhere.add(activite.getLibelle());
                }
                if(activite.getTag_nfc()!=null){
                    where+=" and tag_nfc = '?'";
                    valWhere.add(activite.getTag_nfc());
                }
            }

            Cursor cursor = database.query(table, allColumns, where, valWhere.toArray(new String[]{}), null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Activite comment = cursorToActiite(cursor);
                activites.add(comment);
                cursor.moveToNext();
            }
            cursor.close();
            return activites;
        }
       catch(Exception ex){
            throw new RuntimeException("select all - "+ex.getMessage());
       }
        finally {
            close();
        }
    }

    private Activite cursorToActiite(Cursor cursor) {
        Activite activite = new Activite();
        activite.setIdactivite(cursor.getInt(0));
        activite.setLibelle(cursor.getString(1));
        activite.setImage(cursor.getString(2));
        activite.setPrix(cursor.getDouble(3));
        activite.setPortfollio(cursor.getString(4));
        activite.setDescription(cursor.getString(5));
        activite.setTag_nfc(cursor.getString(6));
        return activite;
    }
}
