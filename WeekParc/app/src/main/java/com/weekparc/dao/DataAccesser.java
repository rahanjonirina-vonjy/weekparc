package com.weekparc.dao;

/**
 * Created by hrahanjo on 2/28/2017.
 */

import com.weekparc.application.ServiceAtlas;

import java.util.Map;

public interface DataAccesser {
    Object getObject(Map<String, String> param, ServiceAtlas.ServiceType servicetype);
    void open() ;
    void close();
}
