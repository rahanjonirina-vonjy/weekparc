package com.weekparc.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weekparc.application.ApplicationConfig;
import com.weekparc.application.ServiceAtlas;
import com.weekparc.manager.Billet;
import com.weekparc.model.Panier;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by hrahanjo on 4/7/2017.
 */

public class PanierDao implements DataAccesser {

    public static final String table = "panier";

    private String[] allColumns = { "idpanier","etat","total"};

    private SQLiteDatabase database;
    private DbHelper dbHelper;
    private static PanierDao instance = null;

    private PanierDao(Context context){
        dbHelper = new DbHelper(context);
    }
    public static PanierDao getInstance(Context context){
        if(instance==null)
            instance=new PanierDao(context);
        return instance;
    }

    @Override
    public Object getObject(Map<String, String> param,ServiceAtlas.ServiceType servicetype) {
        return getPanier();
    }

    @Override
    public void open() {
        try {
            database = dbHelper.getWritableDatabase();
        }
        catch (Exception ex){
            throw new RuntimeException("Open - "+ex.getMessage());
        }
    }

    @Override
    public void close() {
        dbHelper.close();
    }

    public void delete(){
        open();
        try {
            database.delete(table, null, null);
        }
        catch(Exception ex){
            throw ex;
        }
        finally {
            close();
        }
    }
    public Panier getPanier() {
        try {


            Cursor cursor = database.query(table, allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                Panier panier = cursorToPanier(cursor);
                panier.setBillets(BilletDao.getInstance(ApplicationConfig.applicationContext).getAllBillet(null));
                return panier;
            }
            cursor.close();

            ContentValues values = new ContentValues();

            values.put("etat", 0);
            values.put("total", 0.0);

            long insertId = database.insert(table, null, values);
            Panier rep = new Panier();
            rep.setTotal(0.0);
            rep.setEtat(0);
            rep.setBillets(new ArrayList<Billet>());

            return rep;
        }
        catch(Exception ex){
            throw new java.lang.RuntimeException("dao select - "+ex.getMessage());
        }
    }

    private Panier cursorToPanier(Cursor cursor) {
        Panier panier = new Panier();
        panier.setIdpanier(cursor.getInt(0));
        panier.setEtat(cursor.getInt(1));
        panier.setTotal(cursor.getDouble(2));
        return panier;
    }
}