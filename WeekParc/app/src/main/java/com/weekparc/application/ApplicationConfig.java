package com.weekparc.application;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;


import com.weekparc.model.User;
import com.weekparc.model.Visiteur;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class ApplicationConfig extends Application {

    public static String ServeurUrl="http://ultrav.alwaysdata.net/week-parc";
    public static final String base64EncodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs2SQ8pfOskxQgDYSub4Itqu4GBRdSCNu2z/B70aQon1ksm/J/TuiNHTng+98W5ZJIYNiC++eqKw4b+yMRWK+3NjuQmCkVTp4UP9OOB+Qwj311RbBkuxty93fKE+RdrjGj+o3iwVpNayriPTh0hRCpWpjP01CgJz/IYXp4gZGUtGViykG84gjwUbIWKwXZ68mXNjOuD6tsSE084h5PFh/FxF0sZcf0gSQDF5G4nZuqqKi6RBOJZXsuXijIZqEAxrZhmmQlIuTngsXPdiFpd0mnoD9a3oRZI29WwbXYhlEoRfPM/SrQd31tv0Ri4tf5kCXoLQEHg6pxR7GBPm/3kn4jwIDAQAB";
    public static String[] listeServeur={
            "http://ultrav.alwaysdata.net/week-parc"
    };

    public static Context applicationContext;
    public static String applicationCachePath = "";

    public static Visiteur visiteur=null;

    public static User currentUser=null;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        applicationCachePath = applicationContext.getCacheDir().getAbsolutePath() + File.separator + "cache";
    }

}
