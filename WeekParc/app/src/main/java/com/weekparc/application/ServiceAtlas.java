package com.weekparc.application;



import com.weekparc.dao.ActiviteDao;
import com.weekparc.dao.BilletDao;
import com.weekparc.dao.DataAccesser;
import com.weekparc.dao.PanierDao;
import com.weekparc.model.Activite;
import com.weekparc.model.BaseModel;
import com.weekparc.model.Panier;
import com.weekparc.model.User;
import com.weekparc.model.Visiteur;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vonjy-Dev on 18/01/16.
 */
public class ServiceAtlas {

    // ===========================================================
    // Fields
    // ===========================================================
    private static Map<ServiceType,String> mappingPath;
    private static Map<ServiceType,String> mappingUrl;
    private static Map<ServiceType,BaseModel> parser;
    private static Map<ServiceType,DataAccesser> dataAccesser;

    // ===========================================================
    // static code
    // ===========================================================
    static{

        mappingUrl = new HashMap<ServiceType,String>();
        mappingUrl.put(ServiceType.ServiceLogin,"/Login.php");
        mappingUrl.put(ServiceType.ServiceDetailActivite,"/activite.php");
        mappingUrl.put(ServiceType.ServiceListeActivite,"/activites.php");
        mappingUrl.put(ServiceType.ServiceEnvoyePanier,"/envoye_panier.php");//param idactivite=1,4,5&effectif=4,5,6&idvisiteur=6
        mappingUrl.put(ServiceType.ServiceInscription,"/inscription.php");
        mappingUrl.put(ServiceType.ServiceUtiliserBillet,"/utiliser_billet.php");

        parser = new HashMap<ServiceType,BaseModel>();
        parser.put(ServiceType.ServiceLogin,new Visiteur());
        parser.put(ServiceType.ServiceDetailActivite,new Activite());
        parser.put(ServiceType.ServiceListeActivite,new Activite());
        parser.put(ServiceType.ServicePanier,new Panier());
        parser.put(ServiceType.ServiceEnvoyePanier,new Panier());
        parser.put(ServiceType.ServiceInscription,new Visiteur());

        dataAccesser = new HashMap<>();
        dataAccesser.put(ServiceType.ServiceListeActivite, ActiviteDao.getInstance(ApplicationConfig.applicationContext));
        /*dataAccesser.put(ServiceType.ServicePanier, BilletDao.getInstance(ApplicationConfig.applicationContext));
        dataAccesser.put(ServiceType.ServiceDetailActivite, ActiviteDao.getInstance(ApplicationConfig.applicationContext));*/
    }

    public static Object parseFromJson(String jsonResponse, ServiceType serviceType) {
        BaseModel parserB = parser.get(serviceType);
        return parser!=null ? parserB.parseFromJson(jsonResponse,serviceType) : jsonResponse;
    }
    public static Object getDataFromLocalDatabase(Map<String,String> params,ServiceType serviceType){
        DataAccesser accesser = dataAccesser.get(serviceType);
        if(accesser==null)
            return null;
        try {
            accesser.open();
            return accesser.getObject(params,serviceType);
        }catch(Exception ex){
            throw  new RuntimeException("Atlas - "+ex.getMessage());
        }
        finally {
            accesser.close();
        }
    }

    public enum ServiceType{
        ServiceLogin,
        ServiceListeActivite,
        ServiceDetailActivite,
        ServiceAjoutActivite,
        ServicePanier,
        ServiceEnvoyePanier,
        ServiceInscription,
        ServiceUtiliserBillet
    }
    // ===========================================================
    // Public methods
    // ===========================================================
    public static String getUrlForService(ServiceType serviceType)throws Error {
        String url = mappingUrl.get(serviceType);
        return (url!=null) ? ApplicationConfig.ServeurUrl +"/"+ url : null;
    }
    public static String getHttpMethodForService(ServiceType serviceType){
        return "GET";
    }

}
