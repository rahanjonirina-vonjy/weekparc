package com.weekparc.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.weekparc.R;
import com.weekparc.adapter.PortfollioAdapter;
import com.weekparc.application.ApplicationConfig;
import com.weekparc.manager.Billet;
import com.weekparc.manager.BilletManager;
import com.weekparc.manager.DetailActiviteManager;
import com.weekparc.manager.ManagerModel;
import com.weekparc.manager.UitiliseBilletManager;
import com.weekparc.model.Activite;
import com.weekparc.model.Error;
import com.weekparc.utils.UtilAffichage;
import com.weekparc.viewcomponent.ActiviteHomeViewItem;
import com.weekparc.viewcomponent.PortfollioListView;
import com.weekparc.viewcomponent.UrlImageView;

public class DetailActiviteActivity extends AppCompatActivity implements ManagerModel.ManagerListener<Activite>, ImageLoadingListener {

    private static Intent intentDetail=null;
    private static final String EXTRAT_ACTIVITE = "EXTRAT_ACTIVITE";
    private Activite activite = null;
    private DetailActiviteManager manager = null;
    private BilletManager billetmanager;
    private int effectifBilletUtiliser=1;

    private FrameLayout conteneur_image;
    private UrlImageView image =null;
    private ProgressBar progression;
    private TextView labeltitre = null;
    private TextView prix =null;
    private TextView billet =null;
    private TextView description =null;
    private TextView titreLabel =null;
    private TextView prixLabel =null;
    private PortfollioListView listeview;
    private PortfollioAdapter portfollioAdapter;
    private RatingBar rating = null;

    private Button bouton1=null;
    private Button bouton_reduire=null;
    private Button bouton_augmente=null;

    DetailActiviteActivity self=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_activite);

        this.activite=(Activite) getIntent().getSerializableExtra(EXTRAT_ACTIVITE);;
        if (image == null) {

            image = (UrlImageView) findViewById(R.id.urlimage);
            labeltitre = (TextView) findViewById(R.id.titre);
            prix = (TextView) findViewById(R.id.prix);
            billet = (TextView) findViewById(R.id.billet);
            titreLabel = (TextView) findViewById(R.id.titreLabel);
            prixLabel = (TextView) findViewById(R.id.prixLabel);
            billet.setText("0 Billet dans le panier");
            description = (TextView) findViewById(R.id.description_textview_content);
            conteneur_image = ((FrameLayout) findViewById(R.id.conteneur_image));
            progression = ((ProgressBar) findViewById(R.id.progress_image));
            listeview = (PortfollioListView) findViewById(R.id.portfollio);
            bouton1 = (Button) findViewById(R.id.bouton1);
            bouton_reduire = (Button) findViewById(R.id.bouton_reduit);
            bouton_augmente = (Button) findViewById(R.id.bouton_augment);
            rating = (RatingBar) findViewById(R.id.rating_detail);


            bouton_augmente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Billet billet = billetmanager.augmentBillet(activite);
                        if(billet.getEffectif() < 2){
                            self.billet.setText(billet.getEffectif()+" Billet dans le panier");
                        }
                        else {
                            self.billet.setText(billet.getEffectif() + " Billets dans le panier");
                        }
                        bouton_reduire.setEnabled(true);
                    }
                    catch (Exception ex){
                        UtilAffichage.dialogcreate(self,"Erreur",ex.getMessage(),1);
                    }
                }
            });
            bouton_reduire.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        Billet billet = billetmanager.rediureBillet(activite.getIdactivite());
                        if(billet.getEffectif() < 2){
                            self.billet.setText(billet.getEffectif()+" Billet dans le panier");
                        }
                        else {
                            self.billet.setText(billet.getEffectif() + " Billets dans le panier");
                        }
                        if(billet.getEffectif() < 1){
                            bouton_reduire.setEnabled(false);
                        }
                    }
                    catch (Exception ex){
                        UtilAffichage.dialogcreate(self,"Erreur",ex.getMessage(),1);
                    }
                }
            });
            bouton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //new UitiliseBilletManager(utiliseBiletListener).send(activite.getBillet().getIdbillet(),effectifBilletUtiliser);
                    activite.getBillet().setConsommer(activite.getBillet().getConsommer()+effectifBilletUtiliser);
                    refreshData();

                }
            });
            billetmanager = new BilletManager();
        }

        portfollioAdapter = new PortfollioAdapter(this.activite);
        listeview.setAdapter(portfollioAdapter);
        reload();
    }

    public static void affiche(Context context, Activite activite){
        if(intentDetail==null) {
            intentDetail = new Intent(context, DetailActiviteActivity.class);
        }
        intentDetail.putExtra(EXTRAT_ACTIVITE,activite);
        context.startActivity(intentDetail);
    }

    private void reload(){
        if(manager==null){
            manager=new DetailActiviteManager(this);
        }
        manager.refresh(activite);
    }

    @Override
    public void ManagerListenerDidSucced(ManagerModel manager, Activite reponse) {
        this.activite = reponse;
        refreshData();
    }

    @Override
    public void ManagerListenerDidFail(ManagerModel manager, Error error) {
        UtilAffichage.dialogcreate(this,"Erreur", error.getErrorcause(),1);
    }

    private ManagerModel.ManagerListener utiliseBiletListener = new ManagerModel.ManagerListener() {
        @Override
        public void ManagerListenerDidSucced(ManagerModel manager, Object reponse) {
            activite.getBillet().setConsommer(activite.getBillet().getConsommer()+effectifBilletUtiliser);
            refreshData();
        }

        @Override
        public void ManagerListenerDidFail(ManagerModel manager, Error error) {

        }
    };
    private void refreshData(){
        image.setImageUrl(activite.getImage(), this, progression, false);
        labeltitre.setText(activite.getLibelle());
        prixLabel.setText("Prix : ");
        titreLabel.setText("Activité : ");
        prix.setText(activite.getPrix()+" Rs");
        description.setText(Html.fromHtml(activite.getDescription()).toString());
        portfollioAdapter.setActivite(activite);
        portfollioAdapter.notifyDataSetChanged();
        rating.setRating(activite.getNote());

        try {
            Billet billet = billetmanager.getBilletByActivite(activite.getIdactivite());
            if (billet != null) {
                if(billet.getEffectif() < 2){
                    self.billet.setText(billet.getEffectif()+" Billet dans le panier");
                }
                else {
                    self.billet.setText(billet.getEffectif() + " Billets dans le panier");
                }
                if (billet.getEffectif() > 0) {
                    bouton_reduire.setEnabled(true);
                } else {
                    bouton_reduire.setEnabled(false);
                }
            } else {
                bouton_reduire.setEnabled(false);
            }
        }
        catch (Exception ex){
            UtilAffichage.dialogcreate(self,"Erreur",ex.getMessage(),1);
        }
        if(activite.getBillet()!=null) {
            if (activite.getBillet().getEffectif() - activite.getBillet().getConsommer() > 0) {
                this.billet.setText(activite.getBillet().getEffectif() - activite.getBillet().getConsommer() + " Billet(s) dispo");
                bouton_augmente.setEnabled(false);
                bouton_reduire.setEnabled(false);
                bouton1.setEnabled(true);
            }
        }
        else{
            bouton1.setEnabled(false);
        }
    }

    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        adjustImageWithRatio((float)loadedImage.getWidth() / (float)loadedImage.getHeight());
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
    }

    // ===========================================================
    // private method
    // ===========================================================

    private void adjustImageWithRatio(float ratio){

        int width=conteneur_image.getLayoutParams().width;
        int height=(width/ratio<conteneur_image.getLayoutParams().height) ? (int)(width/ratio) : conteneur_image.getLayoutParams().height;
        FrameLayout.LayoutParams absParams = (FrameLayout.LayoutParams)image.getLayoutParams();
        absParams.topMargin = conteneur_image.getLayoutParams().height - height;
        absParams.width = width;
        absParams.height = height;
        image.setLayoutParams(absParams);
    }
}
