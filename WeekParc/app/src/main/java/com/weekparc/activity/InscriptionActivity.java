package com.weekparc.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.weekparc.R;
import com.weekparc.application.ApplicationConfig;
import com.weekparc.manager.InscriptionManager;
import com.weekparc.manager.ManagerModel;
import com.weekparc.model.Error;
import com.weekparc.model.Visiteur;
import com.weekparc.utils.UtilAffichage;

public class InscriptionActivity extends AppCompatActivity implements ManagerModel.ManagerListener<Visiteur> {

    private InscriptionActivity self=this;

    private AutoCompleteTextView mEmailView;
    private AutoCompleteTextView nomView;
    private AutoCompleteTextView prenomView;
    private EditText mPasswordView;
    private EditText mPassword2View;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        nomView = (AutoCompleteTextView) findViewById(R.id.nom);
        prenomView = (AutoCompleteTextView) findViewById(R.id.prenom);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPassword2View = (EditText) findViewById(R.id.password_2);

        Button mEmailSignInButton = (Button) findViewById(R.id.aller_login);

        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetail = new Intent(self, LoginActivity.class);
                self.startActivity(intentDetail);
            }

        });

        Button inscriptotionBouton = (Button) findViewById(R.id.inscrir);
        inscriptotionBouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inscription();
            }
        });
    }
    private void inscription(){
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String password2 = mPassword2View.getText().toString();
        String nom = nomView.getText().toString();
        String prenom = prenomView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }
        if(!password.equalsIgnoreCase(password2)){
            mPassword2View.setError(getString(R.string.error_invalid_password2));
            focusView = mPassword2View;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            Visiteur visiteur = new Visiteur();
            visiteur.setNom(nom);
            visiteur.setPrenom(prenom);
            visiteur.setLogin(email);
            visiteur.setPasse(password);

            new InscriptionManager(this).send(visiteur);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.length() > 4;
    }
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public void ManagerListenerDidSucced(ManagerModel manager, Visiteur reponse) {

        Intent intentDetail = new Intent(self, MainActivity.class);
        self.startActivity(intentDetail);
    }

    @Override
    public void ManagerListenerDidFail(ManagerModel manager, Error error) {
        UtilAffichage.dialogcreate(this,"Erreur",error.getErrorcause(),1);
    }
}
