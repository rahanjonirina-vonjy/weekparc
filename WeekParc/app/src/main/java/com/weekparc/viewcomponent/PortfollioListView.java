package com.weekparc.viewcomponent;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class PortfollioListView extends org.lucasr.twowayview.TwoWayView {
    public PortfollioListView(Context context) {
        super(context);
    }

    public PortfollioListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PortfollioListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        if ( gainFocus && previouslyFocusedRect != null ) {
            requestLayout();
        }
        else {
        }
    }
}