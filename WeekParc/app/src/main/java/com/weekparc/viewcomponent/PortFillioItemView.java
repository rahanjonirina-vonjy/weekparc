package com.weekparc.viewcomponent;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.weekparc.R;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class PortFillioItemView extends FrameLayout implements ImageLoadingListener {

    private FrameLayout conteneur_image = null;
    private UrlImageView image =null;
    private ProgressBar progression = null;

    public PortFillioItemView(Context context) {
        super(context);
    }

    public PortFillioItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PortFillioItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PortFillioItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void affecter(String imageUrl){
        if (image == null) {

            image = (UrlImageView) findViewById(R.id.urlimage);
            progression = ((ProgressBar) findViewById(R.id.progress_image));
        }
        conteneur_image = ((FrameLayout) findViewById(R.id.conteneur_image));
        image.setImageUrl(imageUrl, this, progression, false);
    }

    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        adjustImageWithRatio( (float)loadedImage.getHeight()/(float)loadedImage.getWidth());
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
    }

    // ===========================================================
    // private method
    // ===========================================================

    private void adjustImageWithRatio(float ratio){

        int height=conteneur_image.getLayoutParams().height-1;
        int width=(height/ratio<conteneur_image.getLayoutParams().width) ? (int)(height/ratio) : conteneur_image.getLayoutParams().width;
        FrameLayout.LayoutParams absParams = (FrameLayout.LayoutParams)image.getLayoutParams();
       // absParams.topMargin = conteneur_image.getLayoutParams().height - height;
        absParams.width = width;
        absParams.height = height;
        image.setLayoutParams(absParams);
    }
}
