package com.weekparc.viewcomponent;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.weekparc.R;
import com.weekparc.adapter.CarrouselAdapter;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Vonjy-Dev on 27/01/16.
 */
public class CarrouselView extends LinearLayout {

    android.support.v4.view.ViewPager pager;
    int position=0;
    List<String> images;

    // ===========================================================
    // Constructors
    // ===========================================================

    public CarrouselView(Context context) {
        super(context);
    }

    public CarrouselView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CarrouselView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CarrouselView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    // ===========================================================
    // Public methods
    // ===========================================================

    public void affecter(List<String> images){
        this.images=images;

        CarrouselAdapter adapter=new CarrouselAdapter(images,this.getContext());
        pager=(android.support.v4.view.ViewPager)findViewById(R.id.carrousel_pager);
        pager.setAdapter(adapter);

        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 5000);
    }

    final Handler handler = new Handler();
    final Runnable Update = new Runnable() {
        public void run() {
            if (position == images.size()) {
                position = 0;
            }
            pager.setCurrentItem(position++, true);
        }
    };

    Timer swipeTimer = new Timer();




}
