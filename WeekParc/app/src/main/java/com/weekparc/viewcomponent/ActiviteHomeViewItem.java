package com.weekparc.viewcomponent;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.weekparc.R;
import com.weekparc.model.Activite;
import com.weekparc.utils.UtilAffichage;

/**
 * Created by hrahanjo on 3/30/2017.
 */

public class ActiviteHomeViewItem extends LinearLayout implements View.OnClickListener, ImageLoadingListener {

    private FrameLayout conteneur_image;
    private UrlImageView image =null;
    private ProgressBar progression;
    private ActiviteItemEventListener listener=null;
    private TextView labeltitre = null;
    private TextView prix =null;
    private RatingBar rating =null;

    private Activite activite= null;


    public ActiviteHomeViewItem(Context context) {
            super(context);
    }

    public ActiviteHomeViewItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActiviteHomeViewItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ActiviteHomeViewItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void affect(Activite activite, ActiviteItemEventListener listener){
        try {
            this.setOnClickListener(this);
            this.activite=activite;
            this.listener = listener;
            if (image == null) {

                image = (UrlImageView) findViewById(R.id.urlimage);
                labeltitre = (TextView) findViewById(R.id.titre);
                prix = (TextView) findViewById(R.id.prix);
                conteneur_image = ((FrameLayout) findViewById(R.id.conteneur_image));
                progression = ((ProgressBar) findViewById(R.id.progress_image));
                rating = ((RatingBar) findViewById(R.id.rating_item));
            }
            image.setImageUrl(activite.getImage(), this, progression, false);
            labeltitre.setText(activite.getLibelle());
            rating.setRating(activite.getNote());
            prix.setText(activite.getPrix()+" Rs");
        }
        catch(Exception ex){
            UtilAffichage.dialogcreate(this.getContext(),"Erreur",ex.getMessage(),1);
        }

    }

    @Override
    public void onClick(View v) {
        if(listener!=null)
            listener.didClicked(this.activite);
    }

    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        adjustImageWithRatio((float)loadedImage.getWidth() / (float)loadedImage.getHeight());
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
    }

    // ===========================================================
    // private method
    // ===========================================================

    private void adjustImageWithRatio(float ratio){

        int width=conteneur_image.getLayoutParams().width;
        int height=(width/ratio<conteneur_image.getLayoutParams().height) ? (int)(width/ratio) : conteneur_image.getLayoutParams().height;
        FrameLayout.LayoutParams absParams = (FrameLayout.LayoutParams)image.getLayoutParams();
        absParams.topMargin = conteneur_image.getLayoutParams().height - height;
        absParams.width = width;
        absParams.height = height;
        image.setLayoutParams(absParams);
    }

    public Activite getActivite() {
        return activite;
    }

    public void setActivite(Activite activite) {
        this.activite = activite;
    }

    public interface ActiviteItemEventListener{
        void didClicked(Activite activite);
    }
}
