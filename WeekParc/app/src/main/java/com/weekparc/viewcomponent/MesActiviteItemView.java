package com.weekparc.viewcomponent;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.weekparc.R;
import com.weekparc.manager.Billet;
import com.weekparc.model.Activite;
import com.weekparc.utils.UtilAffichage;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class MesActiviteItemView extends LinearLayout implements View.OnClickListener, ImageLoadingListener {
    private FrameLayout conteneur_image;
    private UrlImageView image =null;
    private ProgressBar progression;
    private ActiviteHomeViewItem.ActiviteItemEventListener listener=null;
    private TextView labeltitre = null;
    private TextView prix =null;
    private TextView billetView =null;
    private Billet billet= null;


    public MesActiviteItemView(Context context) {
        super(context);
    }

    public MesActiviteItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MesActiviteItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MesActiviteItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void affect(Billet billet, ActiviteHomeViewItem.ActiviteItemEventListener listener){
        this.listener=listener;
        this.billet = billet;

        try {
            this.setOnClickListener(this);
            this.listener = listener;
            if (image == null) {

                image = (UrlImageView) findViewById(R.id.urlimage);
                labeltitre = (TextView) findViewById(R.id.libelle);
                prix = (TextView) findViewById(R.id.prix);
                conteneur_image = ((FrameLayout) findViewById(R.id.conteneur_image));
                progression = ((ProgressBar) findViewById(R.id.progress_image));
                billetView = ((TextView) findViewById(R.id.effectif));
            }
            image.setImageUrl(billet.getActivite().getImage(), this, progression, false);
            labeltitre.setText(billet.getActivite().getLibelle());
            this.billetView.setText(billet.getEffectif()+" billets");
            prix.setText(billet.getActivite().getPrix()*billet.getEffectif()+" Rs");
        }
        catch(Exception ex){
            UtilAffichage.dialogcreate(this.getContext(),"Erreur",ex.getMessage(),1);
        }
    }


    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.didClicked(this.billet.getActivite());
        }
    }

    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        adjustImageWithRatio((float)loadedImage.getWidth() / (float)loadedImage.getHeight());
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
    }

    // ===========================================================
    // private method
    // ===========================================================

    private void adjustImageWithRatio(float ratio){

        int width=conteneur_image.getLayoutParams().width;
        int height=(width/ratio<conteneur_image.getLayoutParams().height) ? (int)(width/ratio) : conteneur_image.getLayoutParams().height;
        FrameLayout.LayoutParams absParams = (FrameLayout.LayoutParams)image.getLayoutParams();
        absParams.topMargin = conteneur_image.getLayoutParams().height - height;
        absParams.width = width;
        absParams.height = height;
        image.setLayoutParams(absParams);
    }
}
