package com.weekparc.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.weekparc.R;
import com.weekparc.viewcomponent.UrlImageView;

import java.util.List;


/**
 * Created by Vonjy-Dev on 27/01/16.
 */
public class CarrouselAdapter extends PagerAdapter {

    // ===========================================================
    // Fields
    // ===========================================================

    private List<String> images;
    private LayoutInflater layoutInflater;
    private Context ctx;

    // ===========================================================
    // Constructors
    // ===========================================================

    public CarrouselAdapter(List<String> images, Context ctx){
        this.ctx = ctx;
        this.images = images;
        layoutInflater= (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // ===========================================================
    // method for parents
    // ===========================================================

    @Override
    public int getCount() {
        return (images!=null) ? images.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View resp=layoutInflater.inflate(R.layout.layout_item_carrousel,container,false);
        ((UrlImageView)resp.findViewById(R.id.image_carrousel)).setImageUrl(images.get(position));

        container.addView(resp);

        return resp;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
