package com.weekparc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.weekparc.R;
import com.weekparc.model.Activite;
import com.weekparc.utils.UtilAffichage;
import com.weekparc.viewcomponent.ActiviteHomeViewItem;
import com.weekparc.viewcomponent.PortFillioItemView;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class PortfollioAdapter extends BaseAdapter {

    private String[] images=null;
    private Activite activite;

    public PortfollioAdapter(Activite activite){
        this.setActivite(activite);
    }

    @Override
    public int getCount() {
        return images!=null ? images.length : 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            View retval = convertView;
            if (retval == null){
                LayoutInflater li = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                retval = li.inflate(R.layout.layout_item_portfollio, null);
            }
            PortFillioItemView cell = (PortFillioItemView) retval.findViewById(R.id.portfoffio_container);
            cell.affecter(getImages()[position]);

            return retval;
        }
        catch(Exception ex){
            UtilAffichage.dialogcreate(parent.getContext(),"Erreur potfollio adapter",ex.getMessage(),1);
        }
        return null;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public Activite getActivite() {
        return activite;
    }

    public void setActivite(Activite activite) {
        if(activite.getPortfollio()!=null){
            images = activite.getPortfollio().split(";");
        }
        this.activite = activite;
    }
}
