package com.weekparc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.weekparc.R;
import com.weekparc.manager.Billet;
import com.weekparc.model.Activite;
import com.weekparc.utils.UtilAffichage;
import com.weekparc.viewcomponent.ActiviteHomeViewItem;
import com.weekparc.viewcomponent.MesActiviteItemView;

import java.util.List;

/**
 * Created by hrahanjo on 4/6/2017.
 */

public class MesActiviteListeAdapter extends BaseAdapter {

    private List<Billet> billets;
    private ActiviteHomeViewItem.ActiviteItemEventListener itemlistener = null;

    public MesActiviteListeAdapter(List<Billet> billets,ActiviteHomeViewItem.ActiviteItemEventListener itemlistener){
        this.billets=billets;
        this.itemlistener=itemlistener;
    }

    @Override
    public int getCount() {
        return billets==null ? 0: billets.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            View retval = convertView;
            if (retval == null){
                LayoutInflater li = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                retval = li.inflate(R.layout.layout_mesactivite_item, null);
            }
            MesActiviteItemView cell = (MesActiviteItemView) retval.findViewById(R.id.item);
            cell.affect(getBillets().get(position), itemlistener);

            return retval;
        }
        catch(Exception ex){
            UtilAffichage.dialogcreate(parent.getContext(),"Erreur",ex.getMessage(),1);
        }
        return null;
    }

    public List<Billet> getBillets() {
        return billets;
    }

    public void setBillets(List<Billet> billets) {
        this.billets = billets;
    }
}
