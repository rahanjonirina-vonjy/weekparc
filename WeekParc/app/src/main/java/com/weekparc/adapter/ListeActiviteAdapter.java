package com.weekparc.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.gson.internal.ObjectConstructor;
import com.google.gson.internal.bind.ReflectiveTypeAdapterFactory;
import com.weekparc.R;
import com.weekparc.model.Activite;
import com.weekparc.utils.UtilAffichage;
import com.weekparc.viewcomponent.ActiviteHomeViewItem;

import java.util.List;
import java.util.Map;

/**
 * Created by hrahanjo on 4/4/2017.
 */

public class ListeActiviteAdapter extends BaseAdapter{

    private List<Activite> activites;
    private ActiviteHomeViewItem.ActiviteItemEventListener listener=null;
    private Context context = null;

    public ListeActiviteAdapter(List<Activite> activites,ActiviteHomeViewItem.ActiviteItemEventListener listener,Context context){
        super();
        this.activites = activites;
        this.listener = listener;
        this.context = context;
    }

    public void setActivites(List<Activite> activites){
        this.activites=activites;
    }
    public List<Activite> getActivites(){
        return activites;
    }


    @Override
    public int getCount() {
        return activites==null ? 0 : activites.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            View retval = convertView;
           if (retval == null){
               LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
               retval = li.inflate(R.layout.item_activite_home, null);
           }
             ActiviteHomeViewItem cell = (ActiviteHomeViewItem) retval.findViewById(R.id.activite_item);
            cell.affect(getActivites().get(position), listener);

            return retval;
        }
        catch(Exception ex){
            UtilAffichage.dialogcreate(parent.getContext(),"Erreur",ex.getMessage(),1);
        }
        return null;
    }


}
